<?php

namespace Drupal\ip_api;

/**
 * IpApiParameters service.
 */
class IpApiParameters {

  /**
   * Status.
   *
   * @var string
   */
  protected $status;

  /**
   * IP API Geolocation base URL.
   *
   * @var string
   */
  protected $country;

  /**
   * IP API Geolocation base URL.
   *
   * @var string
   */
  protected $city;

  /**
   * IP API Geolocation base URL.
   *
   * @var string
   */
  protected $countryCode;

  /**
   * IP API Geolocation base URL.
   *
   * @var string
   */
  protected $isp;

  /**
   * IP API Geolocation base URL.
   *
   * @var string
   */
  protected $lat;

  /**
   * IP API Geolocation base URL.
   *
   * @var float
   */
  protected $lon;

  /**
   * IP API Geolocation base URL.
   *
   * @var float
   */
  protected $org;

  /**
   * IP API Geolocation base URL.
   *
   * @var string
   */
  protected $zip;

  /**
   * IP API Geolocation base URL.
   *
   * @var string
   */
  protected $timezone;

  /**
   * Region/state short code (FIPS or ISO).
   *
   * @var string
   */
  protected $region;

  /**
   * IP used for the query.
   *
   * @var string
   */
  protected $query;

  /**
   * Constructs an IpApiParameters object.
   *
   * @param string $ip_api_service
   *   The IP API service.
   */
  public function __construct(string $ip_api_service) {
    $ip_api_service = json_decode($ip_api_service);
    $this->status = $ip_api_service->status ?? NULL;
    $this->country = $ip_api_service->country ?? NULL;
    $this->city = $ip_api_service->city ?? NULL;
    $this->countryCode = $ip_api_service->countryCode ?? NULL;
    $this->isp = $ip_api_service->isp ?? NULL;
    $this->lat = $ip_api_service->lat ?? NULL;
    $this->lon = $ip_api_service->lon ?? NULL;
    $this->org = $ip_api_service->org ?? NULL;
    $this->zip = $ip_api_service->zip ?? NULL;
    $this->timezone = $ip_api_service->timezone ?? NULL;
    $this->region = $ip_api_service->region ?? NULL;
    $this->query = $ip_api_service->query ?? NULL;
  }

  /**
   * Get status.
   *
   * @return string
   *   success or fail.
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Get country name.
   *
   * @return string
   *   Country name(example: United States).
   */
  public function getCountry() {
    return $this->country;
  }

  /**
   * Get city.
   *
   * @return string
   *   City name(example: Mountain View).
   */
  public function getCity() {
    return $this->city;
  }

  /**
   * Get two-letter continent code ISO 3166-1 alpha-2.
   *
   * @return string
   *   Two-letter continent code(example: US).
   */
  public function getCountryCode() {
    return $this->countryCode;
  }

  /**
   * Get region/state short code (FIPS or ISO).
   *
   * @return string
   *   Region/state short code (example: CA or 10).
   */
  public function getRegion() {
    return $this->region;
  }

  /**
   * Get ISP name.
   *
   * @return string
   *   ISP name (example: Google).
   */
  public function getIsp() {
    return $this->isp;
  }

  /**
   * Get latitude.
   *
   * @return float
   *   Latitude (example: 37.4192).
   */
  public function getLatitude() {
    return $this->lat;
  }

  /**
   * Get longitude.
   *
   * @return float
   *   Longitude (example: -122.0574).
   */
  public function getLongitude() {
    return $this->lon;
  }

  /**
   * Get organization name.
   *
   * @return string
   *   Organization name (example: Google).
   */
  public function getOrganization() {
    return $this->org;
  }

  /**
   * Get IP used for the query.
   *
   * @return string
   *   IP (example: 173.194.67.94).
   */
  public function getIp() {
    return $this->query;
  }

  /**
   * Get zip code.
   *
   * @return string
   *   Zip code (example: 94043).
   */
  public function getZip() {
    return $this->zip;
  }

  /**
   * Get timezone (tz).
   *
   * @return string
   *   Timezone (example: America/Los_Angeles).
   */
  public function getTimezone() {
    return $this->timezone;
  }

  /**
   * Service success.
   *
   * @return bool
   *   Whether the request was successful or not.
   */
  public function isRequestSuccessful() {
    if ($this->status === "success") {
      return TRUE;
    }

    return FALSE;
  }

}
