<?php

namespace Drupal\ip_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * IpApiGeolocation service.
 */
class IpApiGeolocation {

  /**
   * IP API Geolocation base URL.
   *
   * @var string
   */
  protected $baseUrl;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Defines the default configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs an IpApiGeolocation object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   Defines a factory for logging channels.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, LoggerChannelFactoryInterface $logger_channel_factory, RequestStack $request_stack) {
    $this->config = $config_factory->get('ip_api.settings');
    $this->logger = $logger_channel_factory->get('ip_api.geolocation');
    $this->httpClient = $http_client;
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->setUrl();
  }

  /**
   * Sets base URL if the IP API key is empty or not.
   */
  protected function setUrl() {

    $ip_api_key = $this->config->get('ip_api_key');
    $ip = $this->currentRequest->getClientIp();

    if ($ip_api_key) {
      $this->baseUrl = 'http://pro.ip-api.com/json/' . $ip . '?key=' . $ip_api_key;
    }
    else {
      $this->baseUrl = 'http://ip-api.com/json/' . $ip;
    }

  }

  /**
   * Query the IP API Geolocation.
   *
   * @return \Drupal\ip_api\IpApiParameters
   *   Return IP API Parameters Object.
   */
  public function callIpApi() {

    try {
      $response = $this->httpClient->get($this->baseUrl);
      return new IpApiParameters($response->getBody()->getContents());
    }
    catch (ClientException $e) {
      $this->logger->error($e);
      return new IpApiParameters($e->getResponse()->getBody()->getContents());
    }
    catch (\Exception $e) {
      $this->logger->error($e);
    }
  }

}
