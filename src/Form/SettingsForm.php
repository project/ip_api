<?php

namespace Drupal\ip_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Ip API settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ip_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ip_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get the configuration.
    $config = $this->config('ip_api.settings');

    $form['ip_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IP API Key'),
      '#description' => 'Empty textfield will call the API base path http://ip-api.com/json',
      '#default_value' => $config->get('ip_api_key'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ip_api.settings')
      ->set('ip_api_key', $form_state->getValue('ip_api_key'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
